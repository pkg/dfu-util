Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: aclocal.m4
Copyright: 1996-2018, Free Software Foundation, Inc.
License: (FSFULLR and/or GPL-2+) with Autoconf-data exception

Files: configure
Copyright: 1992-1996, 1998-2012, Free Software Foundation, Inc.
License: FSFUL

Files: debian/*
Copyright: 2008-2016 Uwe Hermann <uwe@debian.org>
License: GPL-2.0+

Files: doc/dfu-util.1
Copyright: no-info-found
License: GPL-2+

Files: m4/*
Copyright: 1996-2018, Free Software Foundation, Inc.
License: GPL-2+ with Autoconf-data exception

Files: m4/install-sh
Copyright: 1994, X Consortium
License: X11

Files: src/*
Copyright: 2010-2018, Tormod Volden <debian.tormod@gmail.com>
License: GPL-2+

Files: src/dfu.c
Copyright: 2011-2014, Tormod Volden <debian.tormod@gmail.com>
 2005, 2006, Weston Schmidt <weston_schmidt@alumni.purdue.edu>
License: GPL-2+

Files: src/dfu.h
Copyright: no-info-found
License: GPL-2+

Files: src/dfu_file.c
Copyright: 2014-2019, Tormod Volden <debian.tormod@gmail.com>
 2012, Stefan Schmidt <stefan@datenfreihafen.org>
License: GPL-2+

Files: src/dfu_load.c
Copyright: 2014-2016, Tormod Volden <debian.tormod@gmail.com>
 2013, Hans Petter Selasky <hps@bitfrost.no>
 2007, 2008, Harald Welte <laforge@gnumonks.org>
License: GPL-2+

Files: src/dfu_util.c
Copyright: 2016-2019, Tormod Volden <debian.tormod@gmail.com>
 2013, Hans Petter Selasky <hps@bitfrost.no>
 2007, 2008, OpenMoko, Inc.
License: GPL-2+

Files: src/main.c
Copyright: 2013, 2014, Hans Petter Selasky <hps@bitfrost.no>
 2010-2021, Tormod Volden
 2010-2012, Stefan Schmidt
 2007, 2008, OpenMoko, Inc.
License: GPL-2+

Files: src/prefix.c
Copyright: 2014-2020, Tormod Volden <debian.tormod@gmail.com>
 2014, Uwe Bonnes <bon@elektron.ikp.physik.tu-darmstadt.de>
 2013, Hans Petter Selasky <hps@bitfrost.no>
 2011, 2012, Stefan Schmidt <stefan@datenfreihafen.org>
License: GPL-2+

Files: src/quirks.c
Copyright: 2010-2017, Tormod Volden
License: GPL-2+

Files: src/suffix.c
Copyright: 2014-2020, Tormod Volden <debian.tormod@gmail.com>
 2013, Hans Petter Selasky <hps@bitfrost.no>
 2011, 2012, Stefan Schmidt <stefan@datenfreihafen.org>
License: GPL-2+

Files: src/usb_dfu.h
Copyright: 2006, Harald Welte <hwelte@hmw-consulting.de>
License: GPL-2+

Files: * src/Makefile.in
Copyright: 2005-2006 Weston Schmidt <weston_schmidt@alumni.purdue.edu>
 2011-2016 Tormod Volden <debian.tormod@gmail.com>
License: GPL-2.0+
